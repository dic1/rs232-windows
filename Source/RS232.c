#include "RS232.h"

/******************** Declaration of Private Functions ********************/

static void RS232Config(HANDLE rs232Handle, unsigned int baudrate);

/******************** Definition of Public Functions ********************/

HANDLE RS232Open(const char* name, unsigned int baudrate) {
	HANDLE comOpen;

	char* comName = name;

	if (NULL == comName)
		comName = DEFAULT_COM_NAME;

	comOpen = CreateFile(comName, GENERIC_READ | GENERIC_WRITE, 0, 0, CREATE_ALWAYS, 0, 0);

	if (NULL == comOpen) return NULL;

	RS232Config(comOpen, baudrate);

	return comOpen;
}

void RS232Close(HANDLE rs232Handle) {
	CloseHandle(rs232Handle);
}

unsigned int RS232Receive(HANDLE rs232Handle, unsigned char* buffer, unsigned int size) {
	BOOL success;
	unsigned int bytesRead;

	success = ReadFile(rs232Handle, buffer, size, &bytesRead, NULL);

	if (!success) return 0;

	return bytesRead;
}

unsigned int RS232Write(HANDLE rs232Handle, unsigned char* buffer, unsigned int size) {
	BOOL success;
	unsigned int bytesWritten;

	success = WriteFile(rs232Handle, buffer, size, &bytesWritten, NULL);

	if (!success) return 0;

	return bytesWritten;
}

/******************** Definition of Private Functions ********************/

static void RS232Config(HANDLE rs232Handle, unsigned int baudrate) {
	DCB dcbSetting;

	GetCommState(rs232Handle, &dcbSetting);

	dcbSetting.BaudRate = baudrate;

	SetCommState(rs232Handle, &dcbSetting);
}
