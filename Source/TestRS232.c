/**
* File:
*
* Description:
*	Simple tests of the RS232 interface.
*/

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu
//
// Tips for Getting Started:
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file

#include "RS232.h"
#include "stdio.h"
#include <windows.h>

void test1(void);
void testEcho(void);

void main(void) {
	// test1();
	testEcho();
}

void test1(void) {
#define BUFFER_SIZE 20

	unsigned int bytesReceived;
	unsigned char buffer[BUFFER_SIZE + 1];
	HANDLE rs232;

	rs232 = RS232Open("COM3", 9600);

	while (1) {
		bytesReceived = RS232Receive(rs232, buffer, BUFFER_SIZE);

		if (0 < bytesReceived) {
			buffer[bytesReceived] = '\0';
			printf("%s", buffer);
		}
	}

	RS232Close(rs232);
}

void testEcho(void) {
#define BUFFER_SIZE 20

	unsigned int bytesReceived;
	unsigned char buffer[BUFFER_SIZE + 1];
	HANDLE rs232;

	rs232 = RS232Open("COM3", 9600);

	while (1) {

		RS232Write(rs232, "Hallo ", 6);

		bytesReceived = RS232Receive(rs232, buffer, BUFFER_SIZE);

		if (0 < bytesReceived) {
			buffer[bytesReceived] = '\0';
			printf("%s", buffer);
		}
	}
	
	RS232Close(rs232);
}
