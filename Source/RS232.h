/**
* Simple Interface to communicate with the AVR644
*
* Description:
*	Simple interface to the RS232 Win-API to communicate
*	with the microcontroller AVR644 board from the HTBLuVA Salzburg.
* 
* Requirements:
*	Set Eigenschaften/Allgemein/Zeichensatz to "Not Set" und nicht auf UNI-CODE.
*/

#ifndef RS232_H
#define RS232_H

#include "windows.h"

#define DEFAULT_COM_NAME "COM4"

/**
* Function: Rs232Intit
* Description:
*
*
* Parameters:
*	name - The name of the serial interface or NULL
*		   If aName is set to NULL, the default name is used.
*
*	baudrate - The Bausdrate wanted for the serial communication.
*/

HANDLE RS232Open(const char* name, unsigned int baudrate);

void RS232Close(HANDLE rs232Handle);

unsigned int RS232Receive(HANDLE rs232Handle, unsigned char* buffer, unsigned int size);

unsigned int RS232Write(HANDLE rs232Handle, unsigned char* buffer, unsigned int size);

#endif
